# GITHUB MIGRATION
This repository is used for migration repository from GitLab to GitHub.

## Installation

### Git

- [Install Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

### Github CLI

- [Install Github CLI](https://cli.github.com/)

## Prerequisites

- [Setup Gitlab SSH](https://docs.gitlab.com/ee/user/ssh.html)
- [Github CLI Login](https://cli.github.com/manual/gh_auth_login)
- [Modify File Permissions](https://www.howtogeek.com/437958/how-to-use-the-chmod-command-on-linux/)
- CSV file input list all repositories, including source repositories, destination repositories and status.
> Note: CSV file must have header!

## Using

### Migration for 1 repository

```
./git-mirror GITLAB_PATH="<gitlab_ssh_path>" GITHUB_ORG="<github_organization>" GITHUB_REPO="<github_repository>"
```
### Migration for multiple repositories

```
./csv-reader CSV_INPUT="<csv_file>" GITHUB_ORG="<github_organization>"
```